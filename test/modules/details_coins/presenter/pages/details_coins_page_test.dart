import 'package:coins_flutter_mobile_test/core/i.dart';
import 'package:coins_flutter_mobile_test/core/routes/names_routes.dart';
import 'package:coins_flutter_mobile_test/core/utils/number_formatted.dart';
import 'package:coins_flutter_mobile_test/modules/details_coins/presenter/pages/details_coins_page.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_all_coins_usecase.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_user_info.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/pages/home_page.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/store/home_store.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/widgets/coin_widget.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/widgets/image_coin_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:provider/provider.dart';

import '../../../../mocks/list_coins_mock.dart';
import '../../../../mocks/user_mock.dart';

Future<void> _createWidget(WidgetTester tester) async {
  await tester.pumpWidget(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => HomeStore(
            getAllCoinsUseCase: I.get<IGetAllCoinsUseCase>(),
            getUserBalanceUsecase: I.get<IGetUserInfoUsecase>(),
          ),
        ),
      ],
      child: MaterialApp(
        home: const HomePage(),
        routes: {
          Routes.DETAILS_COINS: (ctx) => const DetailsCoinsPage(),
        },
      ),
    ),
  );

  await tester.pump();
}

class GetUserInfoUsecaseMock extends Mock implements IGetUserInfoUsecase {}

class GetAllCoinsUseCaseMock extends Mock implements IGetAllCoinsUseCase {}

void main() {
  final getUserInfoUsecaseMock = GetUserInfoUsecaseMock();
  final getAllCoinsUseCaseMock = GetAllCoinsUseCaseMock();

  setUpAll(
    () {
      I.registerSingleton<IGetUserInfoUsecase>(getUserInfoUsecaseMock);
      I.registerSingleton<IGetAllCoinsUseCase>(getAllCoinsUseCaseMock);

      when(() => getUserInfoUsecaseMock()).thenAnswer(
        (_) async => userMock,
      );

      when(() => getAllCoinsUseCaseMock()).thenAnswer(
        (_) async => listCoinsMock.sublist(0, 3),
      );
    },
  );

  group(
    'DetailsCoinsPage',
    () {
      testWidgets(
        'Should building without exploding',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final allCoins = find.byType(CoinWidget);

          await tester.tap(allCoins.first);

          await tester.pumpAndSettle();

          final homeDetailsPage = find.byType(DetailsCoinsPage);

          expect(homeDetailsPage, findsOneWidget);
        },
      );

      testWidgets(
        'Should building infos of the coin Selected',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final allCoins = find.byType(CoinWidget);

          await tester.tap(allCoins.first);

          await tester.pumpAndSettle();

          final homeDetailsPage = find.byType(DetailsCoinsPage);

          expect(homeDetailsPage, findsOneWidget);

          final nameCoin = find.text(listCoinsMock[0].currencyName!);
          final imageCoin = find.byType(ImageCoinWidget);
          final totalCoin = find.text(
            formattedNumberPtBr(
              double.parse(
                listCoinsMock[0].cotation!,
              ),
            ),
          );
          final descriptionCoin = find.text(listCoinsMock[0].details!.about!);

          expect(nameCoin, findsOneWidget);
          expect(imageCoin, findsOneWidget);
          expect(totalCoin, findsOneWidget);
          expect(descriptionCoin, findsOneWidget);
        },
      );

      testWidgets(
        'Should back for home when Icon Back clicked',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final allCoins = find.byType(CoinWidget);

          await tester.tap(allCoins.first);

          await tester.pumpAndSettle();

          final homeDetailsPage = find.byType(DetailsCoinsPage);

          expect(homeDetailsPage, findsOneWidget);

          final iconBack = find.byType(IconButton);

          expect(iconBack, findsOneWidget);

          await tester.tap(iconBack);

          await tester.pump();

          final homePage = find.byType(HomePage);

          expect(homePage, findsOneWidget);
        },
      );
    },
  );
}
