import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_user_info_data_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/repositories/get_user_info_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/user_mock.dart';

class GetUserInfoDataMock extends Mock implements IGetUserInfoData {}

void main() {
  final getUserInfoDataMock = GetUserInfoDataMock();

  final getAllCoinsRepository = GetUserInfoRepository(
    getUserInfoData: getUserInfoDataMock,
  );

  group(
    'GetUserInfoRepository',
    () {
      test(
        'Should return a Entity User when data returns a Success Value',
        () async {
          when(
            () => getUserInfoDataMock(),
          ).thenAnswer(
            (_) async => userMock,
          );

          final response = await getAllCoinsRepository();

          verify(() => getUserInfoDataMock()).called(1);

          expect(response, isA<User>());
          expect(response, userMock);
        },
      );
      test(
        'Should return a null when data throws a Error',
        () async {
          when(() => getUserInfoDataMock()).thenThrow(Exception('oops'));

          final response = await getAllCoinsRepository();

          verify(() => getUserInfoDataMock()).called(1);

          expect(response, null);
        },
      );
    },
  );
}
