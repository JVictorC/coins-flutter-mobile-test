import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_all_coins_data_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/repositories/get_all_coins_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/list_coins_mock.dart';

class GetAllCoinsDataMock extends Mock implements IGetAllCoinsData {}

void main() {
  final getAllCoinsDataMock = GetAllCoinsDataMock();
  final getAllCoinsRepository = GetAllCoinsRepository(
    getAllCoinsData: getAllCoinsDataMock,
  );

  group(
    'GetAllCoinsRepository',
    () {
      test(
        'Should return a List of the Coin when data returns a Success Value',
        () async {
          when(() => getAllCoinsDataMock())
              .thenAnswer((_) async => listCoinsMock);

          final response = await getAllCoinsRepository();

          verify(() => getAllCoinsDataMock()).called(1);

          expect(response, isA<List<CoinModel>>());
          expect(response, listCoinsMock);
        },
      );
      test(
        'Should return a null when data throws a Error',
        () async {
          when(() => getAllCoinsDataMock()).thenThrow(Exception('oops'));

          final response = await getAllCoinsRepository();

          verify(() => getAllCoinsDataMock()).called(1);

          expect(response, null);
        },
      );
    },
  );
}
