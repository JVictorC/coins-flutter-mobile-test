import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_all_coins_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_all_coins_usecase.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/list_coins_mock.dart';


class GetAllCoinsRepositoryMock extends Mock implements IGetAllCoinsRepository {
}

void main() {
  final getAllCoinsRepositoryMock = GetAllCoinsRepositoryMock();

  final getAllCoinsUseCase = GetAllCoinsUseCase(
    getAllCoinsRepository: getAllCoinsRepositoryMock,
  );

  group(
    'GetAllCoinsUseCase',
    () {
      test(
        'Should return a List of the Coin when repository return a Success value',
        () async {
          when(() => getAllCoinsRepositoryMock()).thenAnswer((_) async => listCoinsMock);

          final response = await getAllCoinsUseCase();

          verify(() => getAllCoinsRepositoryMock()).called(1);

          expect(response, isA<List<CoinModel>>());
          expect(response, listCoinsMock);
        },
      );
    },
  );
}
