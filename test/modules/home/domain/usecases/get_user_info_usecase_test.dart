import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_user_info_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_user_info.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/user_mock.dart';


class GetUserInfoRepositoryMock extends Mock implements IGetUserInfoRepository {
}

void main() {
  final getUserInfoRepositoryMock = GetUserInfoRepositoryMock();

  final getUserInfoUsecase = GetUserInfoUsecase(
    getUserInfoRepository: getUserInfoRepositoryMock,
  );

  group(
    'GetUserInfoUsecase',
    () {
      test(
        'Should return a Entity User when repository return a Success value',
        () async {
          when(() => getUserInfoRepositoryMock()).thenAnswer((_) async => userMock);

          final response = await getUserInfoUsecase();

          verify(() => getUserInfoRepositoryMock()).called(1);

          expect(response, isA<User>());
          expect(response, userMock);
        },
      );
    },
  );
}
