import 'dart:convert';

import 'package:coins_flutter_mobile_test/modules/home/data/get_use_info_data.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks/json_mock.dart';
import '../../../mocks/read_json_file_local_repository_mock.dart';



void main() {

  final getUserInfoData = GetUserInfoData(
    readJsonFileLocalRepository: readJsonFileLocalRepositoryMock,
  );

  group(
    'GetUserInfoData',
    () {
      test(
        'Should convert a json File and Returns a Entity User',
        () async {
          when(
            () => readJsonFileLocalRepositoryMock(
              any(),
            ),
          ).thenAnswer(
            (_) async => json.encode(jsonMock),
          );

          final response = await getUserInfoData();

          verify(
            () => readJsonFileLocalRepositoryMock(any()),
          ).called(1);

          expect(response, isA<User>());

          expect(response!.userBalance, "15.455");
          expect(response.walletId, "c0inS-13435-2342-zksh-34556");
        },
      );
    },
  );
}
