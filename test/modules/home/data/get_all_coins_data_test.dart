import 'dart:convert';

import 'package:coins_flutter_mobile_test/modules/home/data/get_all_coins_data.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks/json_mock.dart';
import '../../../mocks/list_coins_mock.dart';
import '../../../mocks/read_json_file_local_repository_mock.dart';

void main() {
  final getAllCoinsData = GetAllCoinsData(
    readJsonFileLocalRepository: readJsonFileLocalRepositoryMock,
  );

  group(
    'GetAllCoinsData',
    () {
      test(
        'Should convert a json File and Returns a List Coin',
        () async {
          when(
            () => readJsonFileLocalRepositoryMock(
              any(),
            ),
          ).thenAnswer(
            (_) async => json.encode(jsonMock),
          );

          final response = await getAllCoinsData();

          verify(
            () => readJsonFileLocalRepositoryMock(any()),
          ).called(1);

          expect(response, isA<List<CoinModel>>());

          for (var coin in response!) {
            final coinFilter = listCoinsMock
                .where(
                  (c) => c.currencyName == coin.currencyName,
                )
                .toList()[0];

            expect(coinFilter, isA<CoinModel>());
          }
        },
      );
    },
  );
}
