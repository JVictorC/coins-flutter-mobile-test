import 'package:coins_flutter_mobile_test/core/i.dart';
import 'package:coins_flutter_mobile_test/core/routes/names_routes.dart';
import 'package:coins_flutter_mobile_test/core/utils/number_formatted.dart';
import 'package:coins_flutter_mobile_test/modules/details_coins/presenter/pages/details_coins_page.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/details_coins.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_all_coins_usecase.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_user_info.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/pages/home_page.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/store/home_store.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/widgets/coin_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:provider/provider.dart';

import '../../../mocks/list_coins_mock.dart';
import '../../../mocks/user_mock.dart';

class GetUserInfoUsecaseMock extends Mock implements IGetUserInfoUsecase {}

class GetAllCoinsUseCaseMock extends Mock implements IGetAllCoinsUseCase {}

Future<void> _createWidget(WidgetTester tester) async {
  await tester.pumpWidget(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => HomeStore(
            getAllCoinsUseCase: I.get<IGetAllCoinsUseCase>(),
            getUserBalanceUsecase: I.get<IGetUserInfoUsecase>(),
          ),
        ),
      ],
      child: MaterialApp(
        home: const HomePage(),
        routes: {
          Routes.DETAILS_COINS: (ctx) => const DetailsCoinsPage(),
        },
      ),
    ),
  );

  await tester.pump();
}

void main() {
  final getUserInfoUsecaseMock = GetUserInfoUsecaseMock();
  final getAllCoinsUseCaseMock = GetAllCoinsUseCaseMock();

  setUpAll(
    () {
      I.registerSingleton<IGetUserInfoUsecase>(getUserInfoUsecaseMock);
      I.registerSingleton<IGetAllCoinsUseCase>(getAllCoinsUseCaseMock);

      when(() => getUserInfoUsecaseMock()).thenAnswer(
        (_) async => userMock,
      );

      when(() => getAllCoinsUseCaseMock()).thenAnswer(
        (_) async => listCoinsMock.sublist(0, 3),
      );
    },
  );

  group(
    'HomePage',
    () {
      testWidgets(
        'Should building without exploding',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final homePage = find.byType(HomePage);

          expect(homePage, findsOneWidget);
        },
      );

      testWidgets(
        'Should build a balance with users wallet id',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final homePage = find.byType(HomePage);

          expect(homePage, findsOneWidget);

          final balance = find.text(
            formattedNumberPtBr(
              double.parse(userMock.userBalance!),
            ),
          );
          final userWalletId = find.text("c0inS-13435-2342-zksh-34556");

          expect(balance, findsOneWidget);
          expect(userWalletId, findsOneWidget);
        },
      );

      testWidgets(
        'Should build a balance 0.00 and users wallet id empty if useCase returns a Error value',
        (tester) async {
          when(() => getUserInfoUsecaseMock()).thenAnswer(
            (_) async => null,
          );
          await mockNetworkImagesFor(() => _createWidget(tester));

          final homePage = find.byType(HomePage);

          expect(homePage, findsOneWidget);

          final balance = find.text(
            formattedNumberPtBr(
              double.parse('0'),
            ),
          );

          final userWalletId = find.text("c0inS-13435-2342-zksh-34556");

          expect(balance, findsOneWidget);
          expect(userWalletId, findsNothing);
        },
      );

      testWidgets(
        'Should build all coins returns in useCase',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final homePage = find.byType(HomePage);

          expect(homePage, findsOneWidget);

          final allCoins = find.byType(CoinWidget);

          expect(allCoins, findsNWidgets(3));
        },
      );

      testWidgets(
        'Should redirect for details page of the coin clicked',
        (tester) async {
          await mockNetworkImagesFor(() => _createWidget(tester));

          final homePage = find.byType(HomePage);

          expect(homePage, findsOneWidget);

          final allCoins = find.byType(CoinWidget);

          await tester.tap(allCoins.first);

          await tester.pumpAndSettle();

          final homeDetails = find.byType(DetailsCoinsPage);

          expect(homeDetails, findsOneWidget);
        },
      );
    },
  );
}
