import 'package:coins_flutter_mobile_test/modules/home/infra/models/user_model.dart';

import 'json_mock.dart';

final userMock = UserModel.fromMap(jsonMock);