import 'package:coins_flutter_mobile_test/core/domain/repositories/read_json_file_local_repository_interface.dart';
import 'package:mocktail/mocktail.dart';

class ReadJsonFileLocalRepositoryMock extends Mock
    implements IReadJsonFileLocalRepository {}

final readJsonFileLocalRepositoryMock = ReadJsonFileLocalRepositoryMock();
