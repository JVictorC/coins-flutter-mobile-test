import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';

import 'json_mock.dart';

final listCoinsMock =
    (jsonMock['data'] as List).map((e) => CoinModel.fromMap(e)).toList();