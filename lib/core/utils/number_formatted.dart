import 'package:intl/intl.dart';

NumberFormat formatter = NumberFormat.simpleCurrency(locale: 'pt_BR');

String formattedNumberPtBr(double value) => formatter.format(value);
