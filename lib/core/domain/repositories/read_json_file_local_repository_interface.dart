abstract class IReadJsonFileLocalRepository {
  Future<String> call(String path);
}