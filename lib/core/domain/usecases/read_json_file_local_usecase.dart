import 'package:coins_flutter_mobile_test/core/domain/repositories/read_json_file_local_repository_interface.dart';

abstract class IReadJsonFileLocalUseCase {
  Future<String> call(String path);
}

class ReadJsonFileLocalUseCase implements IReadJsonFileLocalUseCase {
  late final IReadJsonFileLocalRepository _readJsonFileLocalRepository;

  ReadJsonFileLocalUseCase({
    required IReadJsonFileLocalRepository readJsonFileLocalRepository,
  }) : _readJsonFileLocalRepository = readJsonFileLocalRepository;

  @override
  Future<String> call(String path) => _readJsonFileLocalRepository(path);
}
