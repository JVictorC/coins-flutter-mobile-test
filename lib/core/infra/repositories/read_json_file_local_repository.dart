import 'package:coins_flutter_mobile_test/core/domain/repositories/read_json_file_local_repository_interface.dart';
import 'package:flutter/services.dart';

class ReadJsonFileLocalRepository implements IReadJsonFileLocalRepository {
  @override
  Future<String> call(String path) async => await rootBundle.loadString(path);
}
