import 'package:coins_flutter_mobile_test/core/routes/names_routes.dart';
import 'package:coins_flutter_mobile_test/modules/details_coins/presenter/pages/details_coins_page.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/pages/home_page.dart';
import 'package:flutter/cupertino.dart';

final Map<String, Widget Function(BuildContext)> allRoutesApp = {
  Routes.HOME_PAGE: (ctx) => const HomePage(),
  Routes.DETAILS_COINS: (ctx) => const DetailsCoinsPage(),
};