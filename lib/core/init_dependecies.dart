import 'package:coins_flutter_mobile_test/core/domain/repositories/read_json_file_local_repository_interface.dart';
import 'package:coins_flutter_mobile_test/core/domain/usecases/read_json_file_local_usecase.dart';
import 'package:coins_flutter_mobile_test/core/i.dart';
import 'package:coins_flutter_mobile_test/core/infra/repositories/read_json_file_local_repository.dart';
import 'package:coins_flutter_mobile_test/modules/home/data/get_all_coins_data.dart';
import 'package:coins_flutter_mobile_test/modules/home/data/get_use_info_data.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_all_coins_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_user_info_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_all_coins_usecase.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_user_info.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_all_coins_data_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_user_info_data_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/repositories/get_all_coins_repository.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/repositories/get_user_info_repository.dart';

void coreDependencies() {
  // Repositories
  I.registerSingleton<IReadJsonFileLocalRepository>(
    ReadJsonFileLocalRepository(),
  );

  //UseCases
  I.registerSingleton<IReadJsonFileLocalUseCase>(
    ReadJsonFileLocalUseCase(
      readJsonFileLocalRepository: I.get<IReadJsonFileLocalRepository>(),
    ),
  );
}

void homeDependencies() {
  // Data
  I.registerSingleton<IGetAllCoinsData>(
    GetAllCoinsData(
      readJsonFileLocalRepository: I.get<IReadJsonFileLocalRepository>(),
    ),
  );

  I.registerSingleton<IGetUserInfoData>(
    GetUserInfoData(
      readJsonFileLocalRepository: I.get<IReadJsonFileLocalRepository>(),
    ),
  );

  //Repositories
  I.registerSingleton<IGetAllCoinsRepository>(
    GetAllCoinsRepository(
      getAllCoinsData: I.get<IGetAllCoinsData>(),
    ),
  );

  I.registerSingleton<IGetUserInfoRepository>(
    GetUserInfoRepository(
      getUserInfoData: I.get<IGetUserInfoData>(),
    ),
  );

  // UseCases
  I.registerSingleton<IGetAllCoinsUseCase>(
    GetAllCoinsUseCase(
      getAllCoinsRepository: I.get<IGetAllCoinsRepository>(),
    ),
  );

  I.registerSingleton<IGetUserInfoUsecase>(
    GetUserInfoUsecase(
      getUserInfoRepository: I.get<IGetUserInfoRepository>(),
    ),
  );
}

initAllDependencies() {
  coreDependencies();
  homeDependencies();
}
