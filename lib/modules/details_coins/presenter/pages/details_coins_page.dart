import 'package:coins_flutter_mobile_test/core/utils/number_formatted.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/widgets/image_coin_widget.dart';
import 'package:flutter/material.dart';

class DetailsCoinsPage extends StatelessWidget {
  const DetailsCoinsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final coin = ModalRoute.of(context)?.settings.arguments as Coin?;

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SafeArea(
          top: true,
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: const Icon(Icons.arrow_back_ios),
                  onPressed: () => Navigator.pop(context),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 35,
                    vertical: 15,
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: ImageCoinWidget(
                                imageUrl: coin?.imageUrl ?? ''),
                          ),
                          const Spacer(),
                          Row(
                            children: [
                              Text(
                                coin?.currencyName ?? '',
                                style: const TextStyle(
                                  fontSize: 35,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                              const SizedBox(width: 15),
                              Text(
                                formattedNumberPtBr(
                                  double.tryParse(coin?.cotation ?? '') ?? 0,
                                ),
                                style: const TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                          const Spacer(),
                        ],
                      ),
                      const Spacer(),
                      Text(
                        coin?.details?.about ?? '',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          height: 1.5
                        ),
                      ),
                      const Spacer(flex: 2),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
