import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_user_info_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_user_info_data_interface.dart';

class GetUserInfoRepository implements IGetUserInfoRepository {
  final IGetUserInfoData _getUserInfoData;

  GetUserInfoRepository({
    required IGetUserInfoData getUserInfoData,
  }) : _getUserInfoData = getUserInfoData;

  @override
  Future<User?> call() async {
    try {
      final response = await _getUserInfoData();

      if (response is User) return response;
    } catch (e) {
      return null;
    }
  }
}
