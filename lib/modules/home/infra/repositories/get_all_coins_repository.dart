import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_all_coins_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_all_coins_data_interface.dart';

class GetAllCoinsRepository implements IGetAllCoinsRepository {
  final IGetAllCoinsData _getAllCoinsData;

  GetAllCoinsRepository({
    required IGetAllCoinsData getAllCoinsData,
  }) : _getAllCoinsData = getAllCoinsData;

  @override
  Future<List<Coin>?> call() async {
    try {
      final response = await _getAllCoinsData();
      if (response is List<Coin>) return response;
    } catch (e) {
      return null;
    }
  }
}
