import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';

abstract class IGetUserInfoData {
  Future<User?> call();
}