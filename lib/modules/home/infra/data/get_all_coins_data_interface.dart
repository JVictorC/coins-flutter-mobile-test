import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';

abstract class IGetAllCoinsData {
  Future<List<Coin>?> call();
}