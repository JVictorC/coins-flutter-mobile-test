
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';

import 'package:coins_flutter_mobile_test/modules/home/infra/models/details_coins_model.dart';




class CoinModel extends Coin {
  CoinModel({
  String? currencyName,
  String? cotation,
  String? symbol,
  String? imageUrl,
  DetailsCoinModel? details,
  }) : super(
    currencyName: currencyName,
    cotation: cotation,
    symbol: symbol,
    imageUrl: imageUrl,
    details: details
  );

  factory CoinModel.fromMap(Map<String, dynamic> map) {
    return CoinModel(
      currencyName: map['currency_name'],
      cotation: map['cotation'],
      symbol: map['symbol'],
      imageUrl: map['image_url'],
      details: DetailsCoinModel.fromMap(map['details']),
    );
  }
}
