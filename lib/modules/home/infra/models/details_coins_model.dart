import 'package:coins_flutter_mobile_test/modules/home/domain/entities/details_coins.dart';

class DetailsCoinModel extends DetailsCoin {
  DetailsCoinModel({
    String? about,
    double? fee,
  }) : super(
          about: about,
          fee: fee,
        );

  factory DetailsCoinModel.fromMap(Map<String, dynamic> map) {
    return DetailsCoinModel(
      about: map['about'],
      fee: map['fee'],
    );
  }
}
