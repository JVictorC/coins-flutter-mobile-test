import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    String? walletId,
    String? userBalance,
  }) : super(
          userBalance: userBalance,
          walletId: walletId,
        );

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      walletId: map['wallet_id'],
      userBalance: map['user_balance'],
    );
  }
}
