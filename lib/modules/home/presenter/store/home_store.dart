import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_all_coins_usecase.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_user_info.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';
import 'package:flutter/cupertino.dart';

class HomeStore extends ChangeNotifier {
  final IGetAllCoinsUseCase _getAllCoinsUseCase;
  final IGetUserInfoUsecase _getUserBalanceUsecase;
  
  HomeStore({
    required IGetAllCoinsUseCase getAllCoinsUseCase,
    required IGetUserInfoUsecase getUserBalanceUsecase,
  })  : _getAllCoinsUseCase = getAllCoinsUseCase,
        _getUserBalanceUsecase = getUserBalanceUsecase;

  List<CoinModel> allCoins = [];

  late User user;

  Future<void> initBalanceUser() async {
    final response = await _getUserBalanceUsecase();
    
    if(response is User) {
      user = response;
    } else {
      user = User();
    }

    notifyListeners();
  }

  Future<void> initCoins() async {
    final response = await _getAllCoinsUseCase();

    if (response is List<CoinModel>) allCoins = response;

    notifyListeners();
  }
}
