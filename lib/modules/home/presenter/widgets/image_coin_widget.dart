import 'package:flutter/material.dart';

class ImageCoinWidget extends StatelessWidget {
  final String imageUrl;

  const ImageCoinWidget({
    Key? key,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      imageUrl,
      fit: BoxFit.fill,
      loadingBuilder: (BuildContext context, Widget child,
          ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return const SizedBox(
          width: 30,
          height: 30,
          child: CircularProgressIndicator(
            color: Colors.grey,
            strokeWidth: 1,
          ),
        );
      },
    );
  }
}
