import 'package:coins_flutter_mobile_test/core/routes/names_routes.dart';
import 'package:coins_flutter_mobile_test/core/utils/number_formatted.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/widgets/image_coin_widget.dart';
import 'package:flutter/material.dart';

import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';

class CoinWidget extends StatelessWidget {
  final CoinModel coin;
  const CoinWidget({
    Key? key,
    required this.coin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 25),
      child: InkWell(
        onTap: () => Navigator.of(context).pushNamed(
          Routes.DETAILS_COINS,
          arguments: coin,
        ),
        child: ListTile(
          leading: ImageCoinWidget(imageUrl: coin.imageUrl ?? ''),
          title: Text(coin.currencyName ?? ''),
          subtitle: Text(
            formattedNumberPtBr(
              double.tryParse(coin.cotation ?? '') ?? 0,
            ),
          ),
          trailing: const Icon(Icons.arrow_forward_ios),
        ),
      ),
    );
  }
}
