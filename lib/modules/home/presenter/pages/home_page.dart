import 'package:coins_flutter_mobile_test/core/utils/number_formatted.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/store/home_store.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/widgets/coin_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    getAllCoins();
  }

  Future<void> getAllCoins() async {
    setState(() => isLoading = true);

    await Provider.of<HomeStore>(context, listen: false).initBalanceUser();

    await Provider.of<HomeStore>(context, listen: false).initCoins();

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    final HomeStore homeStore = Provider.of<HomeStore>(context);

    return isLoading
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : Scaffold(
            body: SafeArea(
              top: true,
              child: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 35,
                        vertical: 25,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Seu Id",
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                              Text(homeStore.user.walletId ?? ''),
                            ],
                          ),
                          const Spacer(),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Seu Saldo",
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                              Text(
                                formattedNumberPtBr(
                                  double.parse(homeStore.user.userBalance ?? '0'),
                                ),
                                style: const TextStyle(
                                  fontSize: 25,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (
                        BuildContext context,
                        int index,
                      ) {
                        final coin = homeStore.allCoins[index];

                        return CoinWidget(coin: coin);
                      },
                      childCount: homeStore.allCoins.length,
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
