import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_all_coins_repository_interface.dart';

abstract class IGetAllCoinsUseCase {
  Future<List<Coin>?> call();
}

class GetAllCoinsUseCase implements IGetAllCoinsUseCase {
  final IGetAllCoinsRepository _getAllCoinsRepository;

  GetAllCoinsUseCase({
    required IGetAllCoinsRepository getAllCoinsRepository,
  }) : _getAllCoinsRepository = getAllCoinsRepository;

  @override
  Future<List<Coin>?> call() async => await _getAllCoinsRepository();
}
