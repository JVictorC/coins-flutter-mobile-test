import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/repositories/get_user_info_repository_interface.dart';

abstract class IGetUserInfoUsecase {
  Future<User?> call();
}

class GetUserInfoUsecase implements IGetUserInfoUsecase {
  final IGetUserInfoRepository _getUserInfoRepository;

  GetUserInfoUsecase({
    required IGetUserInfoRepository getUserInfoRepository,
  }) : _getUserInfoRepository = getUserInfoRepository;

  @override
  Future<User?> call() async => await _getUserInfoRepository();
}
