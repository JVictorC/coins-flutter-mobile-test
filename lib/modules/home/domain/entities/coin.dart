import 'package:coins_flutter_mobile_test/modules/home/domain/entities/details_coins.dart';

class Coin {
  String? currencyName;
  String? cotation;
  String? symbol;
  String? imageUrl;
  DetailsCoin? details;

  Coin({
    this.currencyName,
    this.cotation,
    this.symbol,
    this.imageUrl,
    this.details,
  });
}