class User {
  String? walletId;
  String? userBalance;

  User({
    this.walletId,
    this.userBalance,
  });
}