class DetailsCoin {
  String? about;
  double? fee;

  DetailsCoin({
    this.about,
    this.fee,
  });
}