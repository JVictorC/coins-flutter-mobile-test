import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';

abstract class IGetAllCoinsRepository {
  Future<List<Coin>?> call();
}