import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';

abstract class IGetUserInfoRepository {
  Future<User?> call();
}