import 'dart:convert';

import 'package:coins_flutter_mobile_test/core/domain/repositories/read_json_file_local_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/user.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_user_info_data_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/models/user_model.dart';

class GetUserInfoData implements IGetUserInfoData {
  final IReadJsonFileLocalRepository _readJsonFileLocalRepository;

  GetUserInfoData({
    required IReadJsonFileLocalRepository readJsonFileLocalRepository,
  }) : _readJsonFileLocalRepository = readJsonFileLocalRepository;

  @override
  Future<User?> call() async {
    final jsonData = await _readJsonFileLocalRepository("criptomoedas.json");

    final response = json.decode(jsonData);

    return UserModel.fromMap(response);
  }
}
