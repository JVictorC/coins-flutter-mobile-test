import 'dart:convert';

import 'package:coins_flutter_mobile_test/core/domain/repositories/read_json_file_local_repository_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/entities/coin.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/data/get_all_coins_data_interface.dart';
import 'package:coins_flutter_mobile_test/modules/home/infra/models/coin_model.dart';

class GetAllCoinsData implements IGetAllCoinsData {
  final IReadJsonFileLocalRepository _readJsonFileLocalRepository;

  GetAllCoinsData({
    required IReadJsonFileLocalRepository readJsonFileLocalRepository,
  }) : _readJsonFileLocalRepository = readJsonFileLocalRepository;

  @override
  Future<List<Coin>?> call() async {
    final jsonData = await _readJsonFileLocalRepository("criptomoedas.json");

    final response = json.decode(jsonData);

    final listCoins = (response["data"] as List).map((e) => CoinModel.fromMap(e)).toList();

    return listCoins;
  }
}
