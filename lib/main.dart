import 'package:coins_flutter_mobile_test/core/i.dart';
import 'package:coins_flutter_mobile_test/core/init_dependecies.dart';
import 'package:coins_flutter_mobile_test/core/routes/all_routes_app.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_all_coins_usecase.dart';
import 'package:coins_flutter_mobile_test/modules/home/domain/usecases/get_user_info.dart';
import 'package:coins_flutter_mobile_test/modules/home/presenter/store/home_store.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  initAllDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => HomeStore(
            getAllCoinsUseCase: I.get<IGetAllCoinsUseCase>(),
            getUserBalanceUsecase: I.get<IGetUserInfoUsecase>()
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: allRoutesApp,
      ),
    );
  }
}
